﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ZIZTEST3;

namespace UiDatovePoukazy
{
    /// <summary>
    /// Interaction logic for AddVoucherWindow.xaml
    /// </summary>
    public partial class AddVoucherWindow : Window
    {
        private DB_connect con;
        private List<Receptionist> rec;
        private int receptionistId;
        private string code;
        public AddVoucherWindow()
        {
            InitializeComponent();
            rec = getReceptionists();

        }
        private List<Receptionist> getReceptionists() {
            con = new DB_connect();
            List<Receptionist> rec = new List<Receptionist>();
            SQLiteDataReader reader = con.Select("Select id,username from receptionist;");
            while (reader.Read())
            {
                Receptionist recep = new Receptionist(reader.GetInt32(0), reader.GetString(1));
                rec.Add(recep);
                comBox.Items.Add(recep.name);
            }
            con.CloseConn();
            return rec;
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
           code = generateCode();
        }
        private string generateCode()
        {
            string generatedCode = "";
            int length = 6;
            Random rd = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

            generatedCode= new string(Enumerable.Repeat(chars, length).Select(s => s[rd.Next(s.Length)]).ToArray());

            DB_connect con = new DB_connect();
            SQLiteDataReader reader = con.Select("SELECT voucherId from voucher; ");
            HashSet<string> list = new HashSet<string>();
            while(reader.Read())
                list.Add(reader.GetString(0));

            while(list.Contains(generatedCode))
                generatedCode = new string(Enumerable.Repeat(chars, length).Select(s => s[rd.Next(s.Length)]).ToArray());

            txtBoxCode.Text = generatedCode;
            con.CloseConn();
            return generatedCode;
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            receptionistId =  comBox.SelectedIndex+1;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            DB_connect con = new DB_connect();
            con.Insert("insert into voucher(userId,datum,voucherID) values("+receptionistId+",'"+ DateTime.Now.AddYears(1).ToString("yyyy/MM/dd")+"','"+code+"')");
            con.CloseConn();
            this.Close();
        }


    }
}