﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CV7
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        int sum=0;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void ButtonPlus_Click(object sender, RoutedEventArgs e)
        {
            sum += (Int32.Parse(txtBo.Text));
            txtBl.Text = sum.ToString();
           

        }
        private void ButtonSubtract_Click(object sender, RoutedEventArgs e)
        {
            sum -= (Int32.Parse(txtBo.Text));
            txtBl.Text = sum.ToString();
        }
        private void ButtonMultiply_Click(object sender, RoutedEventArgs e)
        {
            sum *= (Int32.Parse(txtBo.Text));
            txtBl.Text = sum.ToString();
        }
        private void ButtonDivide_Click(object sender, RoutedEventArgs e)
        {
            sum /= (Int32.Parse(txtBo.Text));
            txtBl.Text = sum.ToString();
        }
        private void ButtonReset_Click(object sender, RoutedEventArgs e)
        {
            sum = 0;
            txtBl.Text = "0";
            txtBo.Text = "0";
        }
    }
}
