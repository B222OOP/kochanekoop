﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ZIZTest2
{
    
    public partial class RoomWindow : Window
    {
        public Room room { get; set; }
        public RoomWindow()
        {
            InitializeComponent();
        }

        private void btnCreate_Click(object sender, RoutedEventArgs e)
        {
            int weight;
            int count;
            if (Int32.TryParse(tbWeight.Text, out weight) && Int32.TryParse(tbCount.Text, out count))
            {

                this.room = new Room(Int32.Parse(tbWeight.Text), Int32.Parse(tbCount.Text));
                this.Close();
            }
            else
            {

                MessageBox.Show("Input bad", "Only numbers are allowed in input");
            }
        }

    }
}
