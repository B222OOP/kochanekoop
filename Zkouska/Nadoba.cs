﻿using System;
using System.Collections.Generic;
using System.Windows.Media;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zkouska
{
    public abstract class Nadoba
    {
        public  int maxVolume { get; set; }
        public int volume { get; set; }
        public Color color { get; set; }


        public abstract void Napln(int value);
    }
}
