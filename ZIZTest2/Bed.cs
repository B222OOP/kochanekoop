﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ZIZTest2
{
    public class Bed
    {
       /* public Patient patient { get {
                return patient;
            } set {
                if (value.weight <= maxWeight)
                {
                    this.patient = value; tato p8ovina nefunguje
                }
                else {
                    MessageBox.Show("Patient is too heavy for this bed", "Cant put patient in bed");
                }
            } }
        */
       public Patient patient { get; set; }
        public string name { get; set; }
        public int maxWeight { get; set; }

        public Bed(int maxWeight)
        {
            this.maxWeight = maxWeight;
            patient = null;
        }
        public Bed(int maxWeight,string name)
        {
            this.maxWeight = maxWeight;
            this.name = name;
            patient = null;
        }
        public Bed(Patient patient, int maxWeight)
        {
            if(patient.weight <= maxWeight)
                this.patient = patient;
            this.maxWeight = maxWeight;
            patient = null;
        }
        public override string ToString()
        {
            return name;
        }
        public  string toString() {
            if (patient == null) {
                return "Postel ma nosnost: " + maxWeight + " a nikdo na ni nelezi";
            }
            return "Postel ma nosnost: " + maxWeight + " a lezi na ni pacient: " + patient.name + " " + patient.lastname;
        }
    }
}
