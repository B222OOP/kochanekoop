﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace CV4
{
    public class Tournament
    {
        private  List<Fighter> list;
        private static int numberOfFighters;
        private static int roundCounter;
        private List<List<Fight>> logList;
        private static List<Fight> spider;


        public Tournament(List<Fighter> list)
        {
            this.list = list;
            try
            {
                if (list.Count % 2 != 0)
                    throw new FighterListNotSquareOfTwo();
                numberOfFighters = list.Count;
                roundCounter = 0;

                logList = new List<List<Fight>>();
            }
            catch (FighterListNotSquareOfTwo ex)
            {
                Console.WriteLine("List neni mocnina dvojky ");
                System.Environment.Exit(1);
            }
        }

        private void CreateTournamentTree() {
            spider = new List<Fight>();
            for (int i = 0; i<(numberOfFighters); i+=2)
            {
                spider.Add( new Fight(list[i], list[i + 1]));
                Console.WriteLine(list[i].vypisBojovnika(true));
                Console.WriteLine(list[i+1].vypisBojovnika(true));

            }

        }
        private Fighter GetWinners() {

            if (numberOfFighters == 1)
                return list[0];

            // loggin for printing the spider
           //  for (int i = 0; i < (numberOfFighters / 2); i++)
           //  {
           //      Console.WriteLine(roundCounter + "," + i);
           //      logList[roundCounter][i] = spider[i];
           // }

            list.Clear();
            foreach (Fight f in spider) {
                list.Add(f.returnWinner());
            }
            numberOfFighters = list.Count;
            if (numberOfFighters == 1)
            {
                return list[0];
            }
            roundCounter++;
            return null;
            
        }
        public void Start() {
            do
            {
                CreateTournamentTree();
                foreach (Fight f in spider)
                    f.startFight();
                Console.WriteLine(roundCounter);

            } while (GetWinners() == null);
            Console.WriteLine("Cely turnaj vyhral bojovnik: "+GetWinners().vypisBojovnika(true));
           
        }

        string PrintTournament() {

            string response = "";
            return response;
        }
    }
}
