﻿using CV3;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CV3
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Fighter fighter1 = new Fighter("Marcel", 17, 6, 120);
            Fighter fighter2 = new Fighter("Karel", 11, 13, 145);
            Fight souboj = new Fight(fighter1, fighter2);
            souboj.vypis();

            Console.ReadKey();
        }
    }


}
