﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ZIZTest2
{
    
    public class Room
    {
        private static int counter = 0;
        public List<Bed> beds { get; }
        public string name { get; set; }

        public Room(int maxWeight, int countOfBeds,string name = null) {
            counter++;
            if (name == null)
                name = "Pokoj " + counter;
            this.name = name;
            beds = new List<Bed>();
            for(int i=0;i<countOfBeds;i++)
            {
                beds.Add(new Bed(maxWeight,"L"+i));
            }
        }

        public override string ToString()
        {
            return name;
        }

        public  string toString()
        {
            string response = "Pokoj s nazvem: " + name + "ma postele:\n";
            foreach (Bed bed in beds) {
                response += bed.ToString()+"\n";
            }
            return response;
        }
    }
   
}
