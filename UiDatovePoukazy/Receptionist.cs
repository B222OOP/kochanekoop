﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UiDatovePoukazy
{
    public class Receptionist
    {
        public int id {get; set;}
        public string name {get; set;}

        public Receptionist(int id, string name) {
            this.name = name;
            this.id = id;
        }
    }
}
