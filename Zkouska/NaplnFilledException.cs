﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zkouska
{
    public class NaplnFilledException : Exception
    {
        public NaplnFilledException() {
            
        }

        public NaplnFilledException(string message) : base(message)
        {
        }
    }
}
