﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CV3
{
    public class Fight
    {
        public Fighter fighter1 { get; private set; }
        public Fighter fighter2 { get; private set; }
        public Fight(Fighter fighter1, Fighter fighter2)
        {
            this.fighter1 = fighter1;
            this.fighter2 = fighter2;
        }
        public void startFight() {
            string report = "";
            report += $"Nyni proti sobe bojuje {fighter1.jmeno]} a ";
        }

        public void vypis()
        {
            fighter1.vypisBojovnika();
            fighter2.vypisBojovnika();
            Console.WriteLine(fighter1.hp);
        }

    }
}
