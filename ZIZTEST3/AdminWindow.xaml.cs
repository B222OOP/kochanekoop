﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SQLite;
using System.Windows.Controls.Primitives;

namespace ZIZTEST3
{
    /// <summary>
    /// Interaction logic for AdminWindow.xaml
    /// </summary>
    public partial class AdminWindow : Window
    {
        DB_connect conn = new DB_connect();
        public AdminWindow()
        {
            InitializeComponent();
            
        }

        private void btnMovies_Click(object sender, RoutedEventArgs e)
        {
            DB_connect conn = new DB_connect();
            SQLiteDataReader reader;
            List<Movie> movieList = new List<Movie>();
            reader = conn.Select("SELECT id,nazev ,rezervovany from film;");
            while (reader.Read())
                movieList.Add(new Movie(reader.GetInt32(0),reader.GetString(1),reader.GetInt32(2)));
            conn.CloseConn();
            listBox.ItemsSource = movieList;
            listBox.Items.Refresh();
        }

        private void btnReservations_Click(object sender, RoutedEventArgs e)
        {
            DB_connect conn = new DB_connect();
            SQLiteDataReader reader;
            List<Reservation> reservationList = new List<Reservation>();
            reader = conn.Select("SELECT r.id, u.username, f.nazev, r.datum FROM reservation r JOIN user u ON u.id = userId JOIN film f ON f.id = filmId;");
            while (reader.Read())
                reservationList.Add(new Reservation(reader.GetInt32(0),reader.GetString(1),reader.GetString(2),reader.GetString(3)));
            conn.CloseConn();
            listBox.ItemsSource = reservationList;
            listBox.Items.Refresh();
        }

        private void btnUsers_Click(object sender, RoutedEventArgs e)
        {
            DB_connect conn = new DB_connect();
            SQLiteDataReader reader;
            List<User> userList = new List<User>();
            reader = conn.Select("SELECT user.id, user.username, p.nazev FROM user JOIN privilige p ON p.id  = roleId;");
            while (reader.Read())
                userList.Add(new User(reader.GetInt32(0), reader.GetString(1), reader.GetString(2)));
            conn.CloseConn();
            listBox.ItemsSource = userList;
            listBox.Items.Refresh();
        }

        private void btn_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void btnCreateUser_Click(object sender, RoutedEventArgs e)
        {
            createUser createUser = new createUser();
            createUser.Show();
        }

        private void btnCreateReservation_Click(object sender, RoutedEventArgs e)
        {
            createReservation createReservation = new createReservation();
            createReservation.Show();
                
        }
    }

    
}
