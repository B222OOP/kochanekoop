﻿using System;

namespace DatovePoukazy
{
    public class Voucher
    {
        public string idVoucher { get; set; }
        public DateTime datum { get; set; }
        public int idUser { get; set; }

        public Voucher(string idVoucher, DateTime datum, int idUser)
        {
            this.idVoucher = idVoucher;
            this.datum = datum;
            this.idUser = idUser;

        }
    }
}
