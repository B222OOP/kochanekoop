﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace bagItemTest
{
    public class Bag
    {
        public double maxWeight { get; }
        public double weight { get; set; }
        public int maxCountOfItems { get; }
        public int countOfItems { get; set; }
        private List<Item> list = new List<Item>();

        public Bag(int maxCountOfItems, double maxWeight)
        {
            this.maxCountOfItems = maxCountOfItems;
            this.maxWeight = maxWeight;
            weight = 0;
            countOfItems = 0;
        }

        public bool addItem(Item i)
        {
            if ((((i.weight + weight) > maxWeight) || ((countOfItems + 1) > maxCountOfItems)) || isItemAlreadyInBag(i.name))
            {
                // Console.WriteLine("Vec nejde pridat do batohu");
                return false;
            }

            list.Add(i);
            weight += i.weight;
            countOfItems++;
            return true;


        }
        public bool testIfItemAdded(Item i)
        {
            if ((((i.weight + weight) > maxWeight) || ((countOfItems + 1) > maxCountOfItems)) || isItemAlreadyInBag(i.name))
            {
                // Console.WriteLine("Vec nejde pridat do batohu");
                return false;
            }

            return true;


        }
        public bool removeItem(string name)
        {
            foreach (Item i in list)
            {
                if (string.Equals(i.name, name))
                {
                    list.Remove(i);
                    weight -= i.weight;
                    countOfItems--;
                    return true;
                }

            }
            return false;
        }

        private bool isItemAlreadyInBag(string name)
        {
            foreach (Item i in list)
            {
                if (string.Equals(i.name, name))

                    return true;
            }
            return false;
        }
        public void addList(List<Item> AppendingList)
        {
            
            foreach (Item i in AppendingList)
            {
                if (!testIfItemAdded(i))
                {
                    bool readableInput = false;
                    int choice;
                    while (!readableInput)
                    {
                        Console.WriteLine("Nelze pridat cely list");
                        Console.WriteLine("Chcete pridat nejtezsi veci nebo nejvetsi pocet veci dokud to pujde? ");
                        Console.WriteLine("1) Nejtezsi");
                        Console.WriteLine("2) Nejvic");
                        string input = Console.ReadLine();
                        if (Int32.TryParse(input, out choice) && choice <=2 && choice >=1)
                            readableInput = true;
                        List<Item> SortedList;
                        switch (choice) {
                            case 1:
                                SortedList = AppendingList.OrderByDescending(o => o.weight).ToList();
                                foreach (Item x in SortedList) {
                                    if (testIfItemAdded(x))
                                    {
                                        addItem(x);
                                    }
                                    else
                                    {
                                        break;
                                    }

                                }
                                break;

                            case 2:
                               SortedList = AppendingList.OrderBy(o=>o.weight).ToList();
                                foreach (Item x in SortedList)
                                {
                                    if (testIfItemAdded(x))
                                    {
                                        addItem(x);
                                    }
                                    else
                                    {
                                        break;
                                    }

                                }
                                break;
                        }


                    }
                    break;
                }
                else
                {
                    list.AddRange(AppendingList);
                }

            }

        }
        public string toString()
        {
            string result = "Batoh ma veci:\n";
            foreach (Item i in list)
            {
                result += i.toString() + "\n";
            }
            return result;
        }
        public int freeSpace()
        {
            return (maxCountOfItems - countOfItems);
        }
        public double freeWeight()
        {
            return (maxWeight - weight);
        }
    }
}

