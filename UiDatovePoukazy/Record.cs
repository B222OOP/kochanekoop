﻿using System;

namespace UiDatovePoukazy
{

    public class Record
    {
        public int id { get; set; }
        public string Recepční { get; set; }
        public string kód { get;set; }
        public string platnost { get;set;}

        public bool validní { get; set; }
        public Record(int id,string receptionistName, string code, string date)
        {
            this.id = id;
            this.Recepční = receptionistName;
            this.kód = code;
            this.platnost = date;
            validní = DateTime.Parse(DateTime.Now.ToString("yyyy/MM/dd")) < DateTime.Parse(platnost) ? true : false ;
        }
        public override string ToString()
        {
            return id+ " "+Recepční + "    " + kód + "    " + platnost + ""+ validní;
        }

    }
}
