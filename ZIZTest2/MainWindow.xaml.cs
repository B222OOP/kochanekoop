﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ZIZTest2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Room> roomlist;
        List<Patient> patientlist;
        public MainWindow()
        {
            InitializeComponent();
            roomlist = new List<Room>();
            patientlist = new List<Patient>();



            Patient p1 = new Patient("Doe", 50);
            Patient p2 = new Patient("Jane", 152);
            Patient p3 = new Patient("John", 0);
            patientlist.Add(p2);
            patientlist.Add(p3);
            reloadPatientList();
            Room r1 = new Room(200, 3, "Pokoj 1");
            r1.beds[1].patient = p1;
            roomlist.Add(r1);
            reloadRoomList();
            lbBeds.Visibility = Visibility.Hidden;
            btnTransferFrom.Visibility = Visibility.Hidden;
            btnTransferTo.Visibility = Visibility.Hidden;
            lblTransfer.Visibility = Visibility.Hidden;

        }

        private void listBoxRoom_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Room r = (Room)lbRoom.SelectedItem;
            if (r == null)
                return;
            lbBeds.Items.Clear();
            foreach(Bed b in r.beds)
                lbBeds.Items.Add(b);
            lbBeds.Visibility = Visibility.Visible;
            btnTransferFrom.Visibility = Visibility.Visible;
            btnTransferTo.Visibility = Visibility.Visible;
            lblTransfer.Visibility = Visibility.Visible;
        }
        private void lbBeds_SelectionChanged(object sender, SelectionChangedEventArgs e) { 
        
        }

        private void btnCreateRoom_Click(object sender, RoutedEventArgs e)
        {
            RoomWindow roomWindow = new RoomWindow();
            roomWindow.ShowDialog();
            Room newroom = roomWindow.room;
            roomlist.Add(newroom);
            reloadRoomList();
        }
        private void reloadRoomList() {
            lbRoom.Items.Clear();
            foreach (Room room in roomlist)
            {
                
                lbRoom.Items.Add(room);
            }

        }
        private void reloadPatientList()
        {
            lbPatients.Items.Clear();
            foreach (Patient patient in patientlist)
            {

                lbPatients.Items.Add(patient);
            }

        }
        private void bntCreatePatient_Click(object sender, RoutedEventArgs e)
        {
            PatientWindow patientWindow = new PatientWindow();
            patientWindow.ShowDialog();
            Patient newpatient = patientWindow.patient;
            patientlist.Add(newpatient);
            reloadPatientList();
        }

        private void btnTransferTo_Click(object sender, RoutedEventArgs e)
        {
            Patient patient = (Patient) lbPatients.SelectedItem;
            if (patient == null) {
                MessageBox.Show("You haven't selected patient in patient list", "INPUT BAD");
                return;
            }
            Bed bed = (Bed)lbBeds.SelectedItem;
            if (bed == null)
            {
                MessageBox.Show("You haven't selected bed in bed list", "INPUT BAD");
                return;
            }
            if (bed.patient != null ) {
                MessageBox.Show("Bed isn't empty", "INPUT BAD");
                return;
            }
            if (bed.maxWeight < patient.weight)
            {
                MessageBox.Show("Patient heavier than maximum weight of bed", "INPUT BAD");
                return;
            }
            bed.patient = patient;
            lbBeds.SelectedItem = bed;
            patientlist.Remove(patient);
            reloadPatientList();
            Console.WriteLine("HELOO");
            //lbBeds.SelectedItem;
        }

        private void btnTransferFrom_Click(object sender, RoutedEventArgs e)
        {
            Bed bed = (Bed)lbBeds.SelectedItem;
            if (bed == null)
            {
                MessageBox.Show("You haven't selected bed in bed list", "INPUT BAD");
                return;
            }
            if( bed.patient == null)
            {
                MessageBox.Show("No patient to transfer", "INPUT BAD");
                return;
            }
            Patient patient = bed.patient;
            bed.patient = null;
            lbBeds.SelectedItem = bed;
            patientlist.Add(patient);
            reloadPatientList();
        }
    }
}
