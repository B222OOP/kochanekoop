﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Zkouska
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Fenol fenol;
        public Kyanid kyanid;
        private int fenolAdd=1;

        public MainWindow()
        {
            InitializeComponent();
            fenol = new Fenol(300,0, System.Windows.Media.Color.FromRgb(0,255,0));
            kyanid = new Kyanid(200,0, System.Windows.Media.Color.FromRgb(255, 0, 0));

            lblWarningFenol.Visibility = Visibility.Hidden;
            lblWarningKyanid.Visibility = Visibility.Hidden;

            DispatcherTimer dispatcherTimer;
            dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
            dispatcherTimer.Interval = new TimeSpan(0, 0, 1);
            dispatcherTimer.Start();

        }
        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            

            
            try
            {
                kyanid.Napln(10);
                

            }
            catch(NaplnFilledException ex)
            {
                bool check = chckBoxWarning.IsChecked ?? false;
                if (check)
                    lblWarningKyanid.Visibility = Visibility.Visible;
            }
            rctKyanidFill.Height = kyanid.volume;
            SolidColorBrush brus = new SolidColorBrush(kyanid.color);
            rctKyanidFill.Fill = brus;
            

            try
            {
                fenol.Napln(fenolAdd);
                
                

            }
            catch (NaplnFilledException ex)
            {
                bool check = chckBoxWarning.IsChecked ?? false;
                if (check)
                    lblWarningFenol.Visibility = Visibility.Visible;
            }
            rctFenolFill.Height = fenol.volume;
            SolidColorBrush brus1 = new SolidColorBrush(fenol.color);
            rctFenolFill.Fill = brus1;
            fenolAdd++;
            lblFenol.Content = "naplněno: "+ fenol.volume+"/"+fenol.maxVolume;
            lblKyanid.Content = "naplněno: " + kyanid.volume + "/" + kyanid.maxVolume;
        }

        private void btnEmpty_Click(object sender, RoutedEventArgs e)
        {
            if (radiobtnFenol.IsChecked == true)
            {
                fenol.volume = 0;
                lblWarningFenol.Visibility = Visibility.Hidden;
                lblFenol.Content = "naplněno: " + fenol.volume + "/" + fenol.maxVolume;

                rctFenolFill.Height = fenol.volume;

            }
            else if (radiobtnKyanid.IsChecked == true)
            {
                kyanid.volume = 0;
                lblWarningKyanid.Visibility = Visibility.Hidden;
                lblKyanid.Content = "naplněno: " + kyanid.volume + "/" + kyanid.maxVolume;

                rctKyanidFill.Height = kyanid.volume;
            }

        }

        private void btnSaveState_Click(object sender, RoutedEventArgs e)
        {
            string state = ""+kyanid.volume+","+kyanid.maxVolume+","+kyanid.color+"\n"+ fenol.volume + "," + fenol.maxVolume + "," + fenol.color;
            File.WriteAllText("../../../state.txt", state);
        }

        private void btnLoadState_Click(object sender, RoutedEventArgs e)
        {
            using (StreamReader readtext = new StreamReader("../../../state.txt"))
            {
                string readText = readtext.ReadLine();
                string[] values = readText.Split(',');
                kyanid.volume = int.Parse(values[0]);
                kyanid.maxVolume = int.Parse(values[1]);

                kyanid.color = (Color)System.Windows.Media.ColorConverter.ConvertFromString(values[2]); ;
                readText = readtext.ReadLine();
                values = readText.Split(',');
                fenol.volume = int.Parse(values[0]);
                fenol.maxVolume = int.Parse(values[1]);
                fenol.color = (Color)System.Windows.Media.ColorConverter.ConvertFromString(values[2]);

                rctKyanidFill.Height = kyanid.volume;

            }
        }
    }
}
