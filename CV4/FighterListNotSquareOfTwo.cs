﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CV4
{
    [Serializable]
    public class FighterListNotSquareOfTwo : Exception
    {
        public FighterListNotSquareOfTwo() : base() { }
        public FighterListNotSquareOfTwo(string message) : base(message) { }
        public FighterListNotSquareOfTwo(string message, Exception inner) : base(message, inner) { }

    }
}
