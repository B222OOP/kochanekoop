﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Windows.Media.Media3D;
using System.Xml.Linq;

namespace ZIZTEST3
{
    public class User
    {
        public string username { get; set; }
        public int id { get; set; }
        public string role { get; set; }
        public User(int id, string username,  string role)
        {
            this.username = username;
            this.id = id;
            this.role = role;
        }
        public override string ToString()
        {
            return "Uzivatel: " + username + " id: " + id;
        }

    }
}
