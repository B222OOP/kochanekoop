﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Reflection.PortableExecutable;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ZIZTEST3;

namespace ZIZTEST3
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        void login_button_Click(object sender, RoutedEventArgs e)
        {
            DB_connect db_connection;
            SQLiteDataReader reader;
            db_connection = new DB_connect();
            bool loged = false;
            reader = db_connection.Select("SELECT user.id, username, heslo, p.nazev FROM user JOIN privilige p ON roleId = p.id;");

            while (reader.Read())
            {
                int user_id = reader.GetInt32(0);
                string username = reader.GetString(1);
                string password = reader.GetString(2);
                string role = reader.GetString(3);


                if (txtBoxUsername.Text == "" || txtBoxPassword.Password == "")
                    MessageBox.Show("You have emtpy field", "INPUT BAD");

                if (username == txtBoxUsername.Text && password == txtBoxPassword.Password)
                {
                    if (role == "admin")
                    {
                        AdminWindow admin_window = new AdminWindow();
                        admin_window.Show();
                        this.Close();
                        db_connection.CloseConn();
                        loged = true;
                        break;
                    }
                    if (role == "user")
                    {
                        UserWindow user_window = new UserWindow(user_id);
                        user_window.Show();
                        this.Close();
                        db_connection.CloseConn();
                        loged = true;
                        break;
                    }

                }
 
            }
            if(!loged)
                MessageBox.Show("Bad credentials", "INPUT BAD");
            
        }
    }
}
