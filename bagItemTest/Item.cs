﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bagItemTest
{
    public class Item
    {
        public double weight { get; }
        public string name { get; }

        public Item( string name,double weight) {
            this.name = name;
            this.weight = weight;
        }

        public string toString() {
            return "Vec: "+name + " ma hmotnost: " + weight;
        }
    }
}
