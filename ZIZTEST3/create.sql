CREATE TABLE privilige(
    id INTEGER PRIMARY KEY,
    nazev TEXT NOT NULL UNIQUE
);
CREATE TABLE user(
id INTEGER PRIMARY KEY,
username TEXT NOT NULL UNIQUE,
jmeno TEXT ,
prijmeni TEXT ,
heslo TEXT NOT NULL,
roleId INTEGER NOT NULL,
FOREIGN KEY (roleId)
		REFERENCES privilige(id)
        ON DELETE CASCADE
);
CREATE TABLE film(
id INTEGER PRIMARY KEY,
nazev TEXT NOT NULL UNIQUE,
rezervovany INTEGER NOT NULL,
);
CREATE TABLE reservation(
id INTEGER PRIMARY KEY,
userId INTEGER NOT NULL,
filmID INTEGER NOT NULL,
datum DATETIME NOT NULL
);

insert into privilige(nazev) values('user'),('admin');

insert into user(username,heslo,roleid) values('zerl0k','kedlubna',2),('user','12345',1);