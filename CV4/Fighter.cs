﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CV4
{
    public class Fighter
    {
        public string jmeno { get; private set; }
        public int attack { get; private set; }
        public int deff { get; private set; }
        public int hp { get; private set; }
        public int INT { get; private set; }
        private Random rnd = new Random();

        public Fighter(string jmeno, int attack, int deff, int hp, int INT)
        {
            this.jmeno = jmeno;
            this.attack = attack;
            this.deff = deff;
            this.hp = hp;
            this.INT = INT;
        }
        //Metody pro generovani attacku s randomem 1-attack
        public int genAttack()
        {
            return this.rnd.Next(1, this.attack);
        }

        //metoda pro generovani blocku s randomem 1-deff
        public int genBlock()
        {
            return this.rnd.Next(1, this.deff);
        }

        public int generateINT()
        {
            return this.rnd.Next(this.INT / 2, INT);
        }

        //Metoda pro vypsani vybranych udaju o bojovnikovi - Jmeno, HP. 
        public string vypisBojovnika(bool writeline = false)
        {
            string str = String.Format("Bojovnik: {0} ma jeste: {1} hp", this.jmeno, this.hp);
            // str = "Bojovnik:"+this.jmeno+" ma jeste:"+this.hp.toString()+";
            if (!writeline)
            {
                Console.WriteLine(str+" DIVNEJ VYPIS");
            }
            else
            {
                return str;
            }
            return "";
        }
        // Metodu pro odebrani HP dle hodnoty
        public void dmgToHP(int value)
        {
            this.hp -= value;
        }

    }

}
