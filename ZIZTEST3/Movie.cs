﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZIZTEST3
{
    public class Movie
    {
        public int id { get; set; }
        public string name {get; set;}
        public int reserved {get; set;}
        public Movie(int id, string name, int reserved)
        {
            this.id = id;
            this.name = name;
            this.reserved = reserved;

        }
        public override string ToString()
        {
            return "Film: " + id + " name: " +name + " rezervovany: " + reserved;
        }

    }
}
