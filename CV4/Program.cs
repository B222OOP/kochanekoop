﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CV4
{
    
 
    public class Program
    {
        private static Random random = new Random();

        private static string genRandomString()
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[8];
            

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            var finalString = new String(stringChars);
            return finalString;
        }
        static void Main(string[] args)
        {
            List<Fighter> listOfFighters = new List<Fighter>();
            int countOfFighters = 8;
            Random rd = new Random();
            for (int i = 0; i < countOfFighters; i++)
            {
                string name = genRandomString();
              //  listOfFighters.Add(new Fighter(name, rd.Next(2, 14), 1, rd.Next(3, 20), rd.Next(1, 5)));

                listOfFighters.Add(new Fighter("Fighter"+i, 5,1,5,5));
            }
            Tournament t = new Tournament(listOfFighters);
            t.Start();
        }
    }
}
