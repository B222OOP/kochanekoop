﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ZIZTEST3
{
    /// <summary>
    /// Interaction logic for createReservation.xaml
    /// </summary>
    public partial class createReservation : Window
    {
        DB_connect conn = new DB_connect();
        public createReservation()
        {
            InitializeComponent();
        }

        private void btnCreate_Click(object sender, RoutedEventArgs e)
        {
                conn.Insert("insert into reservation(userId,filmId,datum) values('" + txtBoxUser.Text + "','" + txtBoxFilm.Text + "','"+datePick.SelectedDate.ToString()+"');");
                this.Close();
                conn.CloseConn();
            }
        }
    
}
