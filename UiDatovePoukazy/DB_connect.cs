﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Data.SQLite;

namespace ZIZTEST3
{
    public class DB_connect
    {
        public SQLiteConnection connection;
        public DB_connect()
        {
           connection = new SQLiteConnection("Data Source=../../../db.db; Version = 3; New = True; Compress = True; ");
            try
            {
                connection.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public SQLiteDataReader Select(string command)
        {
            SQLiteCommand cmd = new SQLiteCommand(command, connection);
            SQLiteDataReader reader = cmd.ExecuteReader();
            return reader;
        }
        public void Insert(string command)
        {
            SQLiteCommand cmd = new SQLiteCommand(command, connection);
            SQLiteDataReader reader = cmd.ExecuteReader();
        }
        public void CloseConn()
        {
            this.connection.Close();
        }

        public void Update(string command)
        {
            SQLiteCommand cmd = new SQLiteCommand(command, connection);
            SQLiteDataReader reader = cmd.ExecuteReader();
        }

        public void Delete(string command)
        {
            SQLiteCommand cmd = new SQLiteCommand(command, connection);
            SQLiteDataReader reader = cmd.ExecuteReader();
        }
    }
}

