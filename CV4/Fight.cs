﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CV4
{
    public class Fight
    {
        public Fighter fighter1 { get; private set; }
        public Fighter fighter2 { get; private set; }
        public Fight(Fighter fighter1, Fighter fighter2)
        {
            this.fighter1 = fighter1;
            this.fighter2 = fighter2;
        }

        // Cela logika souboje
        public void startFight()
        {
            //Random rnd = new Random();
            Console.WriteLine("Nyni proti sobe bojuje:{0} a:{1}", this.fighter1.vypisBojovnika(true), this.fighter2.vypisBojovnika(true));
            while (this.fighter1.hp > 0 && this.fighter2.hp > 0)
            {/*
                //Utok fightera1 na 2
                attack(fighter1, fighter2);
                //utok fightera2 na 1
                attack(fighter2, fighter1);*/
                who_attack(fighter1, fighter2);
                //who_attack(fighter2, fighter1);
            }
            returnWinner();
        }

        public void who_attack(Fighter fighter1, Fighter fighter2)
        {/*
            attack(fighter1, fighter2);
            int INT1 = this.fighter1.generateINT();
            int INT2 = this.fighter2.generateINT();

            if (Math.Abs(INT1-INT2) > (INT1+INT2)/2)
            {
                attack(fighter1, fighter2,"Dalsi kriticky utok, protoze je chytrejsi");
            }*/
            int INT1 = this.fighter1.generateINT();
            int INT2 = this.fighter2.generateINT();

            if (INT1 > INT2)
            {
                attack(fighter1, fighter2);
            }
            else
            {
                attack(fighter2, fighter1);
            }

        }

        public void attack(Fighter f1, Fighter f2, string info = "")
        {
            int att = f1.genAttack();
            int blo = f2.genBlock();
            if (blo >= att)
            {
              //  Console.WriteLine("Bojovnik:{0} se mrstne vyhnul utoku od bojovnika:{1} " +
               //     "a stale mu zbyva :{3} hp. {4}", f2.jmeno, f1.jmeno, att, f2.hp, info);
            }
            else
            {
                f1.dmgToHP(att - blo);
               // Console.WriteLine("Bojovnik:{0} udelil bojovnikobi:{1} " +
                 //   "ranu za:{2} a zbyva mu:{3} hp. {4}", f1.jmeno, f2.jmeno, att, f2.hp, info);
            }
        }

        public void vypis()
        {
            fighter1.vypisBojovnika();
            fighter2.vypisBojovnika();
            Console.WriteLine(fighter1.hp);
        }

        //Metodu pro vraceni viteze
        public Fighter returnWinner()
        {
            if (this.fighter1.hp > 0)
            {
          //      Console.WriteLine("Vyhral bojovnik:{0}", this.fighter1.jmeno);
                return fighter1;
            }
        //    Console.WriteLine("Vyhral bojovnik:{0}", this.fighter2.jmeno);
            return fighter2;
        }

    }
}