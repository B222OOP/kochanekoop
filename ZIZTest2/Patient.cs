﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZIZTest2
{
    public class Patient
    {
        public string name { get; set;}
        public string lastname { get; set; }
        public string rodnecislo { get; set; }
        public int weight { get; set; }
        public int length { get; set; }

        public Patient(string lastname, int weight) {
            this.lastname = lastname;
            this.weight = weight;
        }

        public Patient(string name, string lastname, string rodnecislo, int weight, int length)
        {
            this.name = name;
            this.lastname = lastname;
            this.rodnecislo = rodnecislo;
            this.weight = weight;
            this.length = length;
        }
        public override string ToString() {
            return "Pacient: " + name + " " + weight;
        }
        public  string toString()
        {
            return "Pacient: " + name + " " + lastname + " s rodnym cislem: " + rodnecislo + "hmotnost: " + weight + "je vysoky: " + length;
        }

    }
}
