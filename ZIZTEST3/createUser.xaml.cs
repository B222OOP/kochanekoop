﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ZIZTEST3
{
    /// <summary>
    /// Interaction logic for createUser.xaml
    /// </summary>
    public partial class createUser : Window
    {
        DB_connect conn = new DB_connect();
        public createUser()
        {
            InitializeComponent();
        }

        private void btnCreate_Click(object sender, RoutedEventArgs e)
        {
            SQLiteDataReader reader = conn.Select("SELECT username from user where username ='"+txtBoxUsername.Text+"';");
            reader.Read();
            if(reader.HasRows)
                MessageBox.Show("User exists", "INPUT BAD");
            else
            {
                conn.Insert("insert into user(username,heslo,roleId) values('" + txtBoxUsername.Text + "','" + txtBoxPassword.Text + "',1);");
                this.Close();
                conn.CloseConn();
            }

        }
    }
}
