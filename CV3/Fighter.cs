﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CV3
{
    public class Fighter
    {
        public string jmeno { get; private set; }
        public int attack { get; private set; }
        public int deff { get; private set; }
        public int hp { get; private set; }
        private Random rnd = new Random();

        public Fighter(string jmeno, int attack, int deff, int hp)
        {
            this.jmeno = jmeno;
            this.attack = attack;
            this.deff = deff;
            this.hp = hp;

        }
        //Metody pro generovani attacku s randomem 1-attack
        public int genAttack()
        {
            return this.rnd.Next(1, this.attack);
        }

        //metoda pro generovani blocku s randomem 1-deff
        public int genBlock()
        {
            return this.rnd.Next(1, this.deff);
        }

        
        public string vypisBojovnika()
        {
            return $"Bojovnik: {jmeno} ma jeste: {hp} hp";
        }
        public void removeHp(int dmg) {
            hp -= dmg;
        }
        
    }
}
