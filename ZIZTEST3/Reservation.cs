﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZIZTEST3
{
    public class Reservation
    {
        public int id { get; set; }
        public string username { get; set; }
        public string movie { get; set; }
        public string date { get; set; }

       public Reservation(int id, string username, string movie, string date)
        {
            this.id = id;
            this.username = username;   
            this.movie = movie;
            this.date = date;
        }
        public override string ToString()
        {
            return "Rezervace: " + id + " uzivatel: " + username + " film: " + movie+ " do: "+ date.ToString();
        }

    }
}
