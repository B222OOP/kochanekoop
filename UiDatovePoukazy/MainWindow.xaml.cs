﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Windows;
using System.Windows.Controls;
using ZIZTEST3;

namespace UiDatovePoukazy
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //DBConnection con;
        DB_connect conn = new DB_connect();
        SQLiteDataReader reader;
        public MainWindow()
        {
            InitializeComponent();
            refreshRecords();
        }

        private void refreshRecords() {
            List<Record> records = new List<Record>();
            
            reader = conn.Select("SELECT voucher.id,receptionist.username, voucher.voucherID, voucher.datum from receptionist join voucher on voucher.userId = receptionist.id order by datum;");
            while (reader.Read())
            {

                records.Add(new Record(reader.GetInt32(0),reader.GetString(1), reader.GetString(2), reader.GetString(3)));
            }

            dataGrid.ItemsSource = records;
        }
        private void dataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {

        }
        private void MenutItem_Click_Exit(object sender, RoutedEventArgs e)
        {
            System.Environment.Exit(1);
        }
        private void MenutItem_Click_AddVoucher(object sender, RoutedEventArgs e)
        {
            AddVoucherWindow add = new AddVoucherWindow();
            add.Show();
        }
        private void MenutItem_Click_OrderByAsc(object sender, RoutedEventArgs e)
        {
            List<Record> records = new List<Record>();

            reader = conn.Select("SELECT voucher.id,receptionist.username, voucher.voucherID, voucher.datum from receptionist join voucher on voucher.userId = receptionist.id order by datum;");
            while (reader.Read())
            {

                records.Add(new Record(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetString(3)));
            }
            
            dataGrid.ItemsSource = records;
        }
        private void MenutItem_Click_OrderByDesc(object sender, RoutedEventArgs e)
        {
            List<Record> records = new List<Record>();


            reader = conn.Select("SELECT voucher.id,receptionist.username, voucher.voucherID, voucher.datum from receptionist join voucher on voucher.userId = receptionist.id order by datum DESC;");
            while (reader.Read())
            {

                records.Add(new Record(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetString(3)));
            }

            dataGrid.ItemsSource = records;
        }

        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            refreshRecords();
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Chcete opravdu smazat záznam o poukazu?", "Potvrzení smazání poukazu", System.Windows.MessageBoxButton.YesNo);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                int idToDelete=0;
                Record rec = (Record)dataGrid.SelectedItem;
                idToDelete = rec.id;
                conn.Delete("delete from voucher where id = " + idToDelete + ";");
            }
            
            refreshRecords();
        }

        private void dataGrid_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {

        }

        private void txtBoxSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            List<Record> records = new List<Record>();
            reader = conn.Select("SELECT voucher.id,receptionist.username, voucher.voucherID, voucher.datum from receptionist join voucher on voucher.userId = receptionist.id where voucher.voucherId LIKE '%"+txtBoxSearch.Text+"%';");
            while (reader.Read())
            {

                records.Add(new Record(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetString(3)));
            }

            dataGrid.ItemsSource = records;
        }
    }
}
