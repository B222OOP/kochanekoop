﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bagItemTest
{
    public class Program
    {


        static void Main(string[] args)
        {
            Item a = new Item("Rohlik", 5.3);
            Item b = new Item("Trombone",20.1);
            Item c = new Item("Black hole", Double.MaxValue); // Nepujde pridat
            Item d = new Item("Rohlik", 0.1); // nepujde pridat
            
            Bag bag = new Bag(5,26.0);
            bag.addItem(a);
            bag.addItem(b);
            Console.WriteLine(bag.toString());
            if (bag.addItem(c) == false) {
                Console.WriteLine("Vec nejde pridat do batohu");
            }
            if (bag.addItem(d) == false)
            {
                Console.WriteLine("Vec nejde pridat do batohu");
            }
            Console.WriteLine(bag.toString());
            bag.removeItem("Rohlik");

            
            Console.WriteLine(bag.toString());
            List<Item> appendList = new List<Item>();
            appendList.Add(c);
            appendList.Add(d);

            appendList.Add(b);
            appendList.Add(new Item("Houska", 0.2));
            bag.addList(appendList);


            

            //---------------------- Vypsani veci v batohu a informaci o nem
            Console.WriteLine(bag.toString());
            Console.WriteLine(bag.freeSpace());
            Console.WriteLine(bag.freeWeight());
        }
    }
}
