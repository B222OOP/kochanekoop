﻿using System;
using System.Collections.Generic;
using System.Windows.Media;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zkouska
{
    public class Kyanid : Nadoba
    {
        public Kyanid(int maxVolume, int volume, Color color)
        {
            this.maxVolume = maxVolume;
            this.volume = volume;
            this.color = color;
        }
        public override void Napln(int value)
        {

            volume += value;
            if (volume >= 150)
                throw new NaplnFilledException();
        }
    }
}
